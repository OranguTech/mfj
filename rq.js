
var QuoteList = ['Never underestimate the bandwidth of a station wagon full of tapes hurtling down the highway.', 'It\'s not what you can do it\'s what you can get done.', 'Wrong documention is worse than no documentation'];
var QuoteAuthors = ['Andrew S. Tanenbaum',,];



function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }


var QuoteIndex = getRandomInt(QuoteList.length);
var Quote = QuoteList[QuoteIndex];
var Author = QuoteAuthors[QuoteIndex];


if (Author == undefined) {Author = "Unknown"};


console.log('\"'+Quote+'\"'+' --'+Author);

